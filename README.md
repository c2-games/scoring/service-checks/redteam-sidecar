# Red Team Sidecar Container

Purpose built SSH container for Red Team to use as a proxy. Malicious connections from this container will appear to originate from the scoring pod IP. 



## Container environment variables
The only truly necessary environment variable is `TEAMS` as it is used to generate user accounts.
* `PUID=8675309` - The UID used to execute the service check script.
* `PGID=8675309` - The GID used to execute the service check script.
* `USER=redteam` - THE username of the SSH user
* `PASS=marVin93` - The password of the SSH user
* `PUB_KEY=...` - The public key of the SSH user
* `TZ="America/New_York"` - Sets the timezone of the container.


## Docker Deployment Example
```sh
docker create --name redteam-sidecar \
    -e PUID=8675309 \
    -e PGID=8675309 \
    -e USER=username \
    -e PASS=password123 \
    -e PUBLIC_KEY="" \
    -e TZ="America/New_York" \
    registry.gitlab.com/c2-games/scoring/service-checks/redteam-sidecar

docker start redteam-sidecar

docker logs -f redteam-sidecar
```
