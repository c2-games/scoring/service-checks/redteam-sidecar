FROM alpine AS app

ARG BUILD_DATETIME
ARG VCS_REF
ARG VCS_TIMESTAMP
ARG VERSION

LABEL org.label-schema.maintainer="Lucas Halbert <lhalbert@c2games.org>" \
    org.label-schema.build-date=${BUILD_DATETIME} \
    org.label-schema.name="redteam-sidecar" \
    org.label-schema.vendor="c2games" \
    org.label-schema.description="Red Team Sidecar Container" \
    org.label-schema.version=${VERSION} \
    org.label-schema.architecture=amd64 \
    org.label-schema.vcs-ref=${VCS_REF} \
    org.label-schema.vcs-timestamp=${VCS_TIMESTAMP} \
    org.label-schema.vcs-url="https://gitlab.com/c2-games/scoring/service-checks/redream-sidecar" \
    org.label-schema.docker.cmd="docker run -t --rm registry.gitlab.com/c2-games/scoring/service-checks/redteam-sidecar " \
    org.label-schema.docker.cmd.help="docker run --rm registry.gitlab.com/c2-games/scoring/service-checks/redteam-sidecar" \
    org.label-schema.usage="https://gitlab.com/c2-games/scoring/service-checks/redteam-sidecar/README.md" \
    org.label-schema.schema-version="1.0"

COPY .dockerignore .
ADD https://github.com/just-containers/s6-overlay/releases/download/v2.2.0.1/s6-overlay-amd64.tar.gz /tmp/
COPY ./ /

RUN apk -U upgrade --no-cache \
    && \
    apk add --no-cache --update \
    ca-certificates \
    bash \
    shadow \
    iputils \
    tzdata \
    openssh-client \
    dropbear \
    grep \
    netcat-openbsd \
    curl \
    wget \
    python3 \
    py3-pip \
    && \
    tar xzf /tmp/s6-overlay-amd64.tar.gz -C / \
    && \
    rm /tmp/s6-overlay-amd64.tar.gz \
    && \
    groupmod -g 1000 users \
    && \
    useradd -u 911 -U -d /opt/scripts -s /bin/false abc \
    && \
    usermod -G users abc

ENTRYPOINT  ["/init"]
CMD []

